	The goal of the game is to survive all waves and destroy the enemy's base. If your base gets destroyed, it is game over. The health of your base is displayed in the upper left corner. Units can be moved arround by clicking where they should move to. More units can be spawned in with the space bar. Spawning units uses gold. Gold is indicated in the top right corner of the screen. To assist your units, there are several spells you can cast. 

Controls:

	Mouse Click: Send units to mouse.

	Space Bar: Create units at mouse. Units will not spawn if you are too far from your base or there is not enough gold.

	Q Key: Damages enemies near mouse. Cost: 400.

	E Key: Heals your units near mouse. Cost: 200.

	R Key: Special move. Damages enemies, heals you units and gives them a speed boost.