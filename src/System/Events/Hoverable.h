#ifndef HOVERABLE_H
#define HOVERABLE_H

#include "Clickable.h"

class Hoverable: public Clickable{
public:
	virtual ~Hoverable();
	virtual void setWorld( World *world );
	virtual void onClick() = 0;
	virtual void onHover() = 0;
	virtual void notify( Event event );
};

#endif