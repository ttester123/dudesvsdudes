/**
 *\class AQ_Heap
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * This is a wrapper for a heap that holds AQ objects.
 *
 */
#ifndef AQ_HEAP_H
#define AQ_HEAP_H

#include "LOpenGL.h"
#include "../Graphics/Drawing/AQ.h"
#include "Heap.h"

/// Heap class used for storing AQ objects.
class AQ_Heap{
private:
	/**
	 * Used for comparisons during sorting. Use when intializing with make_heap();
	 *
	 *\param a = A pointer to an AQ object to be compared.
	 *\param b = A pointer to an AQ object to be compared.
	 *\return True if a should be placed higher in the heap than b should.
	 */
	bool comparison(AQ* &a, AQ* &b);

	/// Where the events are stored.
	Heap<AQ*> *heap;
public:
	/// Constructor.
	/// Initializes the heap.
	AQ_Heap();

	/// Deconstructor.
	/// For deallocation of the pointer.
	virtual ~AQ_Heap();

	/**
	 * Used to push events to the heap.
	 * 
	 *\param newAQ = A pointer to the AQ object to be added to the heap.
	 */
	void push(AQ* newAQ);

	/**
	 * Used to remove and return from the top of the heap.
	 * 
	 *\return A pointer to the AQ currently at the top of the heap.
	 */ 
	AQ* pop();

	/**
	 * Gets the size of the heap.
	 * 
	 * \return The amount of AQ objects being stored.
	 */
	int getSize();

	/// Clears the heap, removing all nodes.
	void clear();
};

#endif