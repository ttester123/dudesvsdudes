#include "PathfinderHeap.h"
#include "Pathfinder.h"
#include "PathfinderPointer.h"
#include "../../System/Heap.h"

bool compare(PathfinderPointer &a, PathfinderPointer &b) {
	return a.priority < b.priority;
}

PathfinderHeap::PathfinderHeap() {
	heap = new Heap<PathfinderPointer>(&compare);
}

PathfinderHeap::~PathfinderHeap() {
	heap->clear();
	delete heap;
}

void PathfinderHeap::erase() {
	heap->clear();
	delete heap;
	heap = new Heap<PathfinderPointer>(&compare);
}

void PathfinderHeap::push(PathfinderPointer p) {
	heap->push(p);
}

GameObjectPtr PathfinderHeap::pop() {
	return heap->pop().ptr;
}

int PathfinderHeap::getSize() {
	return heap->getSize();
}

void PathfinderHeap::clear() {
	heap->clear();
}

void PathfinderHeap::update(GameObjectPtr n) {
	heap->heapify();
}

bool PathfinderHeap::isEmpty() {
	return heap->getSize() == 0;
}