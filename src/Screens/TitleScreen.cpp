#include "TitleScreen.h"

TitleScreen::~TitleScreen(){
	if( graphics != NULL )
		delete graphics;
}

bool TitleScreen::init(){	
	pathToConfig = "res/Configs/TitleScreen.config";
	if( ScreenShell::init() == false ){
		printf("TitleScreen Error: Failed to initialize from ScreenShell (base class)\n");
		return false;
	}

	graphics = new MainMenuG;
	if( !graphics->init() ){
		printf("TitleScreen Error: Failed to initialize Graphics object graphics.\n");
		return false;
	}

	Options *opts = new Options;
	if( opts->init(world) == false ){
		printf("TitleScreen Error: Failed to initialize Options object opts.\n");
		return false;
	}

	world->addObject( opts );

	return true;
}

void TitleScreen::update(){
	handleEvents();
	if( !transIn && !transOut ){
		graphics->update();
		duringScene->update();
	}
	if( transIn ){
		if( ti->update() == true ){
			transInEnd();
		}
	}
	else if( transOut ){
		if( to->update() == true ){
			transOutEnd();
		}
	}
	world->update();
}

void TitleScreen::render(){
	if(  isActive() && transTo.getType() == NONE || transOut == true ){
		if( !transIn && !transOut ){
			graphics->render(world);
			duringScene->render();
		}
		if( transIn && !transOut ){
			ti->render();
		}
		else if( transOut ){
			to->render();
		}
		world->render();
	}
}

void TitleScreen::handleEvents(){
	if( isActive() ){
		while( getSizeEvents() > 0 ){
			Event tempEvent = pop_event();
			if( tempEvent == START_SCREEN_CHARSEL || tempEvent == START_SCREEN_OPTIONS || tempEvent == START_SCREEN_HELP || tempEvent == START_SCREEN_BESTIARY ){
				if( !transIn ){
					transOut = true;
					transTo = tempEvent;
				}
			}
			else{
				if( transIn && tempEvent == MOUSE_BUTTON_LEFT || tempEvent == KEY_ENTER ){
					transInEnd();
				}
				subject.push_event( tempEvent );
			}
		}
		subject.notifyObservers();
	}
}

void TitleScreen::transInEnd(){
	transIn = false;
	ti->start();
	duringScene->start();

	while( Mix_PlayingMusic() == false )
		world->mp.playMusic( config->getString("music") );
}

void TitleScreen::transOutEnd(){
	subject.push_event( transTo );
	to->start();
	transOut = false;
	subject.notifyObservers();
}

void TitleScreen::setActive( bool value ){
	if( value ){
		transIn = true;
		transTo.setEvent(NONE);

		ti->start();
		duringScene->start();
	}
	else
		transOut = false;
	Screen::setActive(value);
}