#ifndef OPTIONSSCREEN_H
#define OPTIONSSCREEN_H

#include "ScreenShell.h"

class OptionsScreen: public ScreenShell{
protected:
	virtual void handleEvents();
public:
	virtual ~OptionsScreen();
	virtual bool init();
};

#endif