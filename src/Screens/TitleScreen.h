#ifndef TITLESCREEN_H
#define TITLESCREEN_H

#include "ScreenShell.h"
#include "../System/World.h"
#include "../Components/Graphics_Components/Graphics.h"

#include "../Components/Graphics_Components/MainMenuG.h"
#include "../System/Events/Events.h"
#include "../System/Events/Subject.h"
#include "../System/Events/KeyInput.h"
#include "../System/Factory/AnimFactory.h"

#include "../GameObjects/Options.h"

class TitleScreen: public ScreenShell{
private:
	Graphics *graphics;
	
	virtual void handleEvents();
	virtual void transInEnd();
	virtual void transOutEnd();
public:
	virtual ~TitleScreen();
	virtual bool init();
	virtual void update();
	virtual void render();
	virtual void setActive(bool value);
};

#endif