#ifndef NUMBERSHEET_H
#define NUMBERSHEET_H

#include "SpriteSheet.h"
#include "../../System/StringManip.h"

class NumberSheet: public SpriteSheet{
public:
	/**
	 * Call this function before using or else it will crash.
	 * \param Takes a path to a load file that specifies the sprite sheet and coordinates.
	 * \return True if initialized properly, false if it failed.
	 */
	virtual bool load( std::string path );
	virtual void render( AQState state );
	//virtual void draw( World* world );
	virtual void draw( World *world, AQState state, std::string number = "" );
	virtual void draw( World* world, std::string number, int x = 0, int y = 0 );
};

#endif