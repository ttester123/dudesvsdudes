#ifndef TEXTSHEET_H
#define TEXTSHEET_H

#include "SpriteSheet.h"
#include "../../System/LOpenGL.h"

class TextSheet: public SpriteSheet{
private:
	//Background method for figuring out which sprite goes with which character.
	GLuint calculateIndex(char c);
	AQState delta;
	AQState state;
public:
	virtual ~TextSheet();
	//Used to initialize the text to the default text.
	//I'll get this working with true type fonts soon, but for now, this works.
	virtual bool init();

	//Use to init to a specified path and color key. Not very useful at the moment.
	virtual bool init(std::string pathToFont);

	//Basic rendering methods. You can render with text, AQState, or by specifying the values.
	virtual void render(GLfloat x, GLfloat y, std::string text, GLfloat scale = 1.f);
	virtual void render(std::string text);
	virtual void render(AQState state);

	//Use draw functions instead of render functions.
	//Draw functions will take advantage of priorities and layers.
	virtual void draw( World* world, std::string text );
	virtual void draw( World *world, AQState state );
	virtual void draw( World *world );

	virtual void setState( AQState newState );
	virtual void addDelta( AQState newState );
	
	//Returns dimentions of the text, when you've just got to know.
	LFRect getTextDimensions(std::string text, GLfloat scale = 1.f);
};

#endif