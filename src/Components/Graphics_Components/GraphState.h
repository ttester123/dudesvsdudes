#ifndef GRAPHSTATE_H
#define GRAPHSTATE_H

#include "../../System/LOpenGL.h"
#include "../../System/Color.h"

class GraphState{
public:
		//GraphState();

		//Position to render to
		GLfloat x;
		GLfloat y;

		//Width and Height of grid
		GLfloat width;
		GLfloat height;

		//Number of blocks
		int horzCount;
		int vertCount;

		//Line thickness
		GLfloat thickness;

		//Color of background and lines
		Color background;
		Color line;
};

#endif