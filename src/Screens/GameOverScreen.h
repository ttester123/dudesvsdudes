#ifndef GAMEOVERSCREEN_H
#define GAMEOVERSCREEN_H

#include "ScreenShell.h"

class GameOverScreen: public ScreenShell{
protected:
	virtual void handleEvents();
public:
	virtual bool init();
};

#endif