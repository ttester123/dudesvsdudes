/**
 *\class ObjectsManager
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Class that is a wrapper for GameObjects. An instance of this lives in the World Object.
 *
 */
#ifndef OBJECTSMANAGER_H
#define OBJECTSMANAGER_H

class World;
#include "../System/LOpenGL.h"
#include "GameObject.h"
#include "GameObjectPtr.h"

class ObjectsManager{
private:
	/**
	 * This is the vector where the gameObjects are being stored.
	 */
	std::vector<GameObjectPtr> objectsVec;
public:
	virtual ~ObjectsManager();

	/**
	 * Used to initialize the objects manager. Use to load initial units.
	 */
	bool init( World *world );

	/**
	 * Updates all GameObjects.
	 *\param world = A pointer to a World object.
	 */
	void update();

	/**
	 * Pushes AQ's of all GameObjects into the deck.
	 *\param world = A pointer to the world whose deck you are pushing AQs to.
	 */
	void render();

	/**
	 * Adds an GameObject into the ObjectsManager vector.
	 *\param gameObject = Pointer to the GameObject to be added to the vector.
	 *\return GameObjectPtr to GameObject.
	 */
	GameObjectPtr addObject(GameObject *gameObject);

	/**
	 * Adds an GameObject into the ObjectsManager vector.
	 *\param gameObject = Pointer to the GameObject to be added to the vector.
	 *\return GameObjectPtr to GameObject.
	 */
	GameObjectPtr addObject(GameObjectPtr gameObject);

	/**
	 * Removes a GameObject from the objectsManager.
	 *\param removed = GameObject that you want removed.
	 */
	int removeObject(GameObject *removed);

	/**
	 * \return The vector of that stores the GameObjects.
	 */
	std::vector<GameObjectPtr> getObjectsVec();
};

#endif