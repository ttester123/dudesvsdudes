#ifndef LVERTEXDATA_H
#define LVERTEXDATA_H

#include "LTexCoord.h"
#include "LVertex.h"

struct LVertexData{
	LTexCoord coord;
	LVertex position;
};

#endif