/**
 *\class StringManip
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Static class for specialized string manipulation.
 *
 */
#ifndef STRINGMANIP_H
#define STRINGMANIP_H

#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>

class StringManip{
protected:
	static std::stringstream ss;
public:
	/**
	 * Use to split a string by a delimiter.
	 *
	 *\param str = The string to be split.
	 *\param delimiters = The delimiter by which to split the string.
	 *\return A vector of strings that is str split by the delimiters.
	 */
	static std::vector<std::string> split( std::string str, std::string delimiters);

	/**
	 * Checks if a certain character is a delimiter.
	 *
	 *\param c = Character to check.
	 *\param delimiter = A string of the delimiters.
	 *\return True if the char c is a delimiter.
	 */
	static bool isDelimiter( char c, std::string delimiter );
	
	/**
	 * Use when opening a file to initialize the SpriteSheet.
	 *
	 *\param stringVec = A vector of strings to be converted to doubles. The vector is usually obtained from the split function.
	 *\return A vector of doubles that is converted from the stringVec.
	 */
	static std::vector<double> convertStringVecToDouble( std::vector<std::string> stringVec );

	/**
	 * Removes whitespace characters from a string.
	 * \param str = The string to remove whitespace characters from.
	 * \return The string parameter, but without the whitespaces.
	 */
	static std::string removeWhitespace( std::string str );

	/**
	 * Joins a vector of strings.
	 * \param strVec = The vector of strings to join.
	 * \param delim = A string that is placed inbetween each of the strings when they are joined together.
	 * \param begin = The first index of the vector to start joining.
	 * \param end = The index in which to stop joining. -1 signals to go until the end.
	 * \return A string that is the result of joining the strings in the string vector with the given parameters.
	 */
	static std::string join(std::vector<std::string> strVec, std::string delim = "", int begin = 0, int end = -1);
	static std::string convertToInt( std::string str );
	static std::string itoa( int num );
};

#endif