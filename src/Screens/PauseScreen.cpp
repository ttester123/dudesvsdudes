#include "PauseScreen.h"

PauseScreen::~PauseScreen(){}

bool PauseScreen::init(){
	pathToConfig = "res/Configs/PauseScreen.config";
	return ScreenShell::init();
}

void PauseScreen::handleEvents(){
	if( isActive() ){
		while( eventHeap->getSize() > 0 ){
			Event event = eventHeap->pop();
			switch( event.getType() ){
				case KEY_q:
					if( !transIn ){
						transOut = true;
						transTo = RESET;
					}
					break;
				case KEY_ENTER:
					if( !transIn ){
						transOut = true;
						transTo = KILL_SCREEN;
					}
					break;
			}
		}
		subject.notifyObservers();
	}
}