#include "NodeHeap.h"
#include "Node.h"
#include "NodePointer.h"
#include "../../System/Heap.h"

bool compare(NodePointer &a, NodePointer &b) {
	return a.ptr->cost < b.ptr->cost;
}

NodeHeap::NodeHeap() {
	heap = new Heap<NodePointer>(&compare);
}

NodeHeap::~NodeHeap() {
	heap->clear();
	delete heap;
}

void NodeHeap::erase() {
	heap->clear();
	delete heap;
	heap = new Heap<NodePointer>(&compare);
}

void NodeHeap::push(Node *newNode) {
	heap->push(NodePointer(newNode));
}

Node* NodeHeap::pop() {
	return heap->pop().ptr;
}

int NodeHeap::getSize() {
	return heap->getSize();
}

void NodeHeap::clear() {
	heap->clear();
}

void NodeHeap::update(Node *n) {
	heap->heapify();
}

bool NodeHeap::isEmpty() {
	return heap->getSize() == 0;
}