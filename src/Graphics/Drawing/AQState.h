#ifndef AQSTATE_H
#define AQSTATE_H

#include "../../System/LOpenGL.h"
#include "../../System/Color.h"

//This is a class instead of a struct because initialization needs to be ensured with these elements.
class AQState{
public:
	AQState();
	void resetToDefault();
	void clear();

	virtual AQState operator+(AQState state);
	virtual AQState operator*(int times);
	virtual void operator+=(AQState delta);
	virtual void operator*=(int times);

	//X and Y hold the positions to render to on the screen.
	GLfloat x;
	GLfloat y;

	//W and H hold the width and height of the given Texture or Sprite.
	GLfloat w;
	GLfloat h;

	//If the texture is a sprite sheet, it will use this to determine which sprite.
	//If not, no harm is done. Default to 0.
	GLuint index;

	//Holds which layer this Augmented Quad wants to be placed in. Defaults to 0.
	GLuint layer;
	
	//Used to determine the order to render in the layers object.
	GLuint priority;

	//Holds the degrees at which to rotate the image. Defaults to 0.
	GLfloat degrees;

	//ScaleX and ScaleY are for scaling the texture verticaly and horizontally.
	GLfloat scaleX;
	GLfloat scaleY;

	//Scale is for scaling the entire texture. Typically used for scaling text.
	GLfloat scale;

	//This is used if the texture is text. Stores the message.
	std::string text;

	//Use this for setting color.
	Color color;

	bool drawFromCenter;
	bool drawAbsolute;
};

#endif