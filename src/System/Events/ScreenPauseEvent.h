#ifndef SCREENPAUSEEVENT_H
#define SCREENPAUSEEVENT_H

#include "Event.h"

class ScreenPauseEvent: public Event{
private:
	bool active;
public:
	ScreenPauseEvent();
	bool isActive();
	void setActive( bool active );
};

#endif