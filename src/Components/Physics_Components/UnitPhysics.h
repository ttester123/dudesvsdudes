#ifndef UNITPHYSICS_H
#define UNITPHYSICS_H

#include "Physics.h"
#include "../../GameObjects/GameObject.h"
#include "../../GameObjects/Unit.h"
#include "../../System/LOpenGL.h"
#include <cmath>

class UnitPhysics : public Physics {
private:
	GLfloat deltaX, deltaY;
	GLfloat targetX, targetY;

	GLfloat distance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
	GLfloat trueDistance(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);


public:
	UnitPhysics();
	void update(GameObject *self, World *world);
	void moveTo(GLfloat x, GLfloat y, Unit *self);
	void teleportTo(GLfloat x, GLfloat y, Unit *self);
	friend class Unit;
};

#endif