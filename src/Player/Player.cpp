#include <vector>
#include <iostream>

#include "Player.h"
#include "../GameObjects/Point.h"
#include "../GameObjects/Board.h"
#include "../GameObjects/GameObjectFactory.h"
#include "../Components/AI_Components/Dijkstra.h"
#include "GameObjectSorter.h"
#include "../System/Heap.h"

using std::vector;

Player::Player(Board *w, int t) {
	world = w;
	team = t;
	squads.reserve(11);
	makeSquad();
	squads[0].setRallyPoint(Point(63,8));
	squads[0].desiredCount = -1;
	gold = 10000;
	euforia = 100;
	balance = 0;
	world->subject.addObserver(this, Event(UNIT_DEAD));
	cheat = false;
	activeSquads = 0;
	character = 0;
}

void Player::cleanUp() {
	world->subject.removeObserver(this, Event(UNIT_DEAD));
}

bool Player::makeSquad() {
	Squad s = Squad(world);
	s.desiredCount = 1;
	squads.push_back(s);
	activeSquads++;
	return true;
}

void Player::setActive(int i, bool value) {
	if(i <1 || i >= squads.size())
		return;
	if(squads[i].isActive() == value)
		return;
	if(squads[i].isActive()) {
		squads[i].setActive(value);
		activeSquads--;
	} else {
		squads[i].setActive(value);
		activeSquads++;
	}
}

int Player::makeUnit(int type, int count, Point p) {
	int cost = world->UnitFactory.getCost(type);

	if(cost*count > gold && !cheat)
		count = gold/cost;
	vector<Point> list = dijkstraFill(world->getCostGrid(),p,count+50);
	//std::cout << list.size() << "\n";
	int made = 0;
	for(int i=0; i<list.size() && made<count; i++) {
		GameObjectPtr dude = world->UnitFactory.makeUnit(type,team,list[i]);
		if(dude.getPtr() != NULL) {
			squads[0].addUnit(dude);
			made++;
			gold -= cost;
		}
	}
	return made;
}

int Player::makeTower(int type, Point p) {
	int cost = world->UnitFactory.getCost(type);
	if(cost > gold && !cheat)
		return 0;
	
	GameObjectPtr dude = world->UnitFactory.makeTower(5,team,p);
	if(dude.getPtr() != NULL) {
		gold -= cost;
	}
	return 1;
}


void Player::balanceSquads() {
	//std::cout << balance << ' ';
	if(balance == 0) {
		//calculate how many shares there are
		shares = 0;
		for(int i=1; i<squads.size(); i++)
			if(squads[i].isActive()) {
			shares += squads[i].getPriority();
		}
		unitPerShare = getUnitCount() / shares;
		//tell each squad what they should get
		for(int i=1; i<squads.size(); i++){
			
			if(squads[i].isActive())
				squads[i].desiredCount = unitPerShare * squads[i].getPriority();
			else
				squads[i].desiredCount = 0;
		}
	} else if(balance == 3) {
		if(squads.size() > 1 && squads[0].getSize() >0) {
			//equaly distribute units amongst squads
			int index=1;
			int count = 0;
			while(squads[0].getSize() > 0) {
				GameObjectPtr dude = squads[0].popUnit();
				if(squads[index].isActive()) {
					squads[index].addUnit(dude);
					count ++;
				}	
				if(count > squads[index].desiredCount - squads[index].getSize()+1) {
					count = 0;
					index ++;
					if(index == squads.size())
						index = 1;
				}
			}
		}
	} else if(balance == 1) {
		//std::cout << "Balancing\n";
		for(int i=0; i<squads.size(); i++){
			squads[i].checkAlive();
		}
	} else if(balance == 2) {
		vector<GameObjectSorter> unitList;
		for(int i=0; i<squads.size(); i++){
			//GameObjectPtr temp = GameObjectPtr();
			Squad &cur = squads[i];
			if(cur.desiredCount < cur.getSize()-1) {
				for(int j=0; j<cur.getSize(); j++) {
					unitList.push_back(GameObjectSorter(cur.get(j),0,i));
				}
			}
		}
		Heap<GameObjectSorter> unitHeap;
		unitHeap.setCompare(compareGameObject);
		for(int i=1; i<squads.size(); i++){
			//GameObjectPtr temp = GameObjectPtr();
			Squad &cur = squads[i];
			if(cur.isActive() == false) continue;
			if(cur.desiredCount > cur.getSize()) {
				for(int j=0; j<unitList.size(); j++) {
					Unit *dude = dynamic_cast<Unit*>(unitList[j].ptr.getPtr());
					if(dude != NULL) {
						int cost = cur.getGrid()->get(dude->getLocation());
						if(cur.getRush() ) {
							cost = cost * 10 / dude->speed;
						}
						if(cur.getDefend() ) {
							cost = cost + 500 - dude->health;
						}
						if(cur.getRanged() ) {
							if(dude->attackRange > 1)
								cost += 10000;
						}
						unitList[j].cost = cost;
					}
					else
						unitList[j].cost = 100000000;
				}
				unitHeap = Heap<GameObjectSorter>(unitList);
				unitHeap.setCompare(compareGameObject);
				unitHeap.heapify();
				for(int j=cur.getSize(); j<cur.desiredCount && unitHeap.getSize() > 0; j++) {
					//std::cout << j << ' ' << cur.desiredCount << '\n';
					GameObjectSorter dude = unitHeap.pop();
					cur.addUnit(dude.ptr);
					squads[dude.index].removeUnit(dude.ptr);

				}
			}
		}
	} 
	balance++;
	if(balance > 3){
		balance = 0;
	}
	//std::cout << balance << '\n';
}

void Player::areaAffect(Point center, int range, Event effect) {
	int x, y;
	y = -range;
	x = 0;
	while(y<=range) {
		world->passEvent(effect,Point(center.x+x, center.y+y));
		x++;
		if(x+abs(y) > range) {
			y++;
			x = abs(y) - range;
		}
	}
}

void Player::areaAffect(Point center, int range, Event effect, float modifier, int cooldown) {
	effect.data1.resize(2);
	effect.data1[0] = *((int*) &modifier);
	effect.data1[1] = cooldown;
	areaAffect(center, range, effect);
}

int Player::getGold() {
	return gold;
}

int Player::getEuforia() {
	return euforia;
}

int Player::getTeam() {
	return team;
}

int Player::getUnitCount() {
	int count = 0;
	for(int i=0; i<squads.size(); i++) {
		count += squads[i].getSize();
	}
	return count;
}

void Player::superUpdate() {
	//gold++;
	handleEvents();
}

void Player::handleEvents() {
	while( eventHeap->getSize() > 0 ){
		Event event = eventHeap->pop();
		int *data;
		switch(event.getType()){
		case UNIT_DEAD:
			data = (int*) &(event.data);
			if(*data == team)
				break;
			gold += *(data+1);
			//std::cout << "Unit gold recieved\n";
			break;
		default:
			childHandleEvent(event);
		}
	}
}

void Player::childHandleEvent(Event event) {

}

void Player::changeGold(int n) {
	gold += n;
}


void Player::setGold(int n) {
	gold = n;
}

void Player::setCharacter( int num ){
	character = num;
}

int Player::getCharacter(){
	return character;
}