#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "CostGrid.h"
#include "Node.h"
#include "NodeHeap.h"
#include "../../GameObjects/Board.h"

#include <vector>

using std::vector;

/**
 * Takes the passed in costgrid and makes each point
 * the path cost to reach it.
 *\param grid = grid to modify
 *\param start = relative point to start from
 */
void dijkstra(CostGrid &grid, Point start);

/**
 * Makes a list of points flooding out from start
 *\param grid = grid to pathfind from
 *\param start = start point
 *\param count = amount of points to fill
 *\return list of points
 */
vector<Point> dijkstraFill(CostGrid grid, Point start, int count);

/**
 * Finds the lowest cost point next to start that is open
 *\param board = board to check for openings
 *\param grid = cost grid to pathfind from
 *\param start = startpoint
 *\return Point to move to
 */
Point nextPoint(Board *board, CostGrid &grid, Point start, Point reject = Point());


class Dijksta {
private:
	Heap<Node> nodeHeap;
	CostGrid grid;
	int width, height;
	bool **visited;
	int depth;
	Board *board;

public:
	Dijksta(Board *b, CostGrid g);

	/**
	 * Initializes search.
	 *\param start = start point
	 *\param depth = amount of nodes to search, -1 = whole grid
	 *\return true if start point is valid
	 */
	bool initSearch(Point start, int depth);

	/**
	 * Finds the first open space
	 *\param nodes = amount of nodes to search
	 *\return open space, (-1,-1) if none found
	 */
	Point findOpen(int nodes);

	/**
	 * Finds first GameObject
	 *\param nodes = amount of nodes to search
	 *\return first object found, NULL if not found
	 */
	GameObjectPtr findGameObject(int nodes);

	/**
	 * Finds first unit of indicated team
	 *\param team = team to search for
	 *\return unit fount, NULL if not found
	 */
	GameObjectPtr findUnit(int team, int nodes);
};



#endif