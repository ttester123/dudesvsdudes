/**
 *\class Color
 *
 *\author Shane Satterfield
 *
 *\date 2013/03/02
 *
 *\brief
 *
 * Class to simplify colors by bundling them together.
 *
 */
#ifndef COLOR_H
#define COLOR_H

#include "LOpenGL.h"

//Make sure when rendering, cal glColor4ub(r,g,b,a) and that a is set to 255.
//If you call glColor4f(r,g,b,a) you will get black.
class Color{
public:
	/**
	 * Default constructor for initializing the Color object.
	 *
	 *\param r = GLubyte (0 to 255) for red value.
	 *\param g = GLubyte (0 to 255) for green value.
	 *\param b = GLubyte (0 to 255) for blue value.
	 *\param a = GLubyte (0 to 255) for alpha value.
	 */	
	Color(GLubyte r = 255, GLubyte g = 255, GLubyte b = 255, GLubyte a = 255);

	/**
	 * Function for setting the color all in one.
	 *
	 *\param r = GLubyte (0 to 255) for red value.
	 *\param g = GLubyte (0 to 255) for green value.
	 *\param b = GLubyte (0 to 255) for blue value.
	 *\param a = GLubyte (0 to 255) for alpha value.
	 */
	void setColor(GLubyte r = 255, GLubyte g = 255, GLubyte b = 255, GLubyte a = 255);
	
	/// GLubyte (0 to 255) for red value.
	GLubyte r;

	/// GLubyte (0 to 255) for green value.
	GLubyte g;

	/// GLubyte (0 to 255) for blue value.
	GLubyte b;

	/// GLubyte (0 to 255) for alpha value.
	GLubyte a;
};

#endif