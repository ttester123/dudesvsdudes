#include "Basic_Quad.h"

Basic_Quad::~Basic_Quad(){}

void Basic_Quad::render(AQState state){

	glTranslatef(state.x, state.y, 0.f);

	glColor4ub(state.color.r, state.color.g, state.color.b, state.color.a);

	glBegin(GL_QUADS);
		glVertex2f(0.f, 0.f);
		glVertex2f(state.w, 0.f);
		glVertex2f(state.w, state.h);
		glVertex2f(0.f, state.h);
	glEnd();

	glColor4f(1.f, 1.f, 1.f, 1.f);

	glTranslatef( -state.x, -state.y, 0.f);
}