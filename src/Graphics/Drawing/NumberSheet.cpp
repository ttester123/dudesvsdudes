#include "NumberSheet.h"

bool NumberSheet::load( std::string path ){
	if( open( path ) == false ){
		return false;
	}
	return true;
}

void NumberSheet::render( AQState state ){

	LFRect stretch = {state.scaleX, state.scaleY, 0.f, 0.f};
	if( state.scale != 1.f ){
		stretch.x = state.scale;
		stretch.y = state.scale;
	}

	if(state.drawAbsolute)
		glTranslatef( -Constants::getPanned().x, -Constants::getPanned().y, 0.f );

	glTranslatef( state.x, state.y, 0.f );
	
	if(state.color.r != 255 || state.color.g != 255 || state.color.b != 255 || state.color.a != 255)
		glColor4ub(state.color.r, state.color.g, state.color.b, state.color.a);

	if(stretch.x != 1.f || stretch.y != 1.f)
		glScalef(stretch.x, stretch.y, 1.f);
	
	//if(state.drawFromCenter)
		//glTranslatef( (getClip(index).w)/2.f, (getClip(index).h)/2.f, 0.f );

	if(state.degrees != 0.f)
		glRotatef(state.degrees, 0.f, 0.f, 1.f);

	int xTraversal = 0;
	state.text = StringManip::convertToInt( state.text );
	int index = 0;
	for( int i = 0; i < state.text.size(); i++){
		index =  state.text[i] - 48;
		SpriteSheet::render( index );
		glTranslatef( getClip(index).w, 0, 0 );
		xTraversal += getClip(index).w;
	}
	glTranslatef( -xTraversal, 0, 0 );

	if(state.degrees != 0.f)
		glRotatef(-state.degrees, 0.f, 0.f, 1.f);

	//if(state.drawFromCenter)
		//glTranslatef( (getClip(index).w)/-2.f, (getClip(index).h)/-2.f, 0.f );

	if(stretch.x != 1.f || stretch.y != 1.f)
		glScalef(1.f/stretch.x, 1.f/stretch.y, 1.f);

	if(state.color.r != 255 || state.color.g != 255 || state.color.b != 255 || state.color.a != 255)
		glColor4f(1.f, 1.f, 1.f, 1.f);
	
	glTranslatef( -state.x, -state.y, 0.f );

	if(state.drawAbsolute)
		glTranslatef( Constants::getPanned().x, Constants::getPanned().y, 0.f );
}


void NumberSheet::draw( World* world, std::string number, int x, int y ){
	AQState newState;
	newState.text = number;
	newState.x = x;
	newState.y = y;

	pushToDeck( newState, world );
}

void NumberSheet::draw( World *world, AQState state, std::string number ){
	if( number != "" ){
		state.text = number;
	}
	pushToDeck( state, world );
}