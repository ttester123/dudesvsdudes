#include "MainMenuG.h"

MainMenuG::~MainMenuG(){
	if( logo != NULL ){
		delete logo;
		logo = NULL;
	}

	if( logo != NULL ){
		delete bq;
		bq = NULL;
	}
}

bool MainMenuG::init(){
	
	logo = new Animated;
	if( logo->loadAnimation("res/Animations/logo.anim") == false ){
		return false;
	}

	bq = new Basic_Quad;
	
	AQState state;
	state.w = Constants::SCREEN_WIDTH;
	state.h = Constants::SCREEN_HEIGHT;
	state.color.setColor( 235, 30, 35 );
	state.priority = 40;
	bq->applyState( state );

	return true;
}

void MainMenuG::update(){
	logo->update();
}

void MainMenuG::render( World *world ){
	bq->pushToDeck( 0, world );
	logo->draw(world);
}