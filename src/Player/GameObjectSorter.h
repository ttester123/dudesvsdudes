#ifndef GAMEOBJECTSORTER_H
#define GAMEOBJECTSORTER_H

#include "../GameObjects/GameObjectPtr.h"

struct GameObjectSorter {
	GameObjectPtr ptr;
	int cost;
	int index;
	GameObjectSorter(GameObjectPtr p, int c, int i) {
		ptr = p;
		cost = c;
		index = i;
	}
	GameObjectSorter(const GameObjectSorter &other) {
		ptr = other.ptr;
		cost = other.cost;
		index = other.index;
	}

	GameObjectSorter& operator = (const GameObjectSorter &other) {
		ptr = other.ptr;
		cost = other.cost;
		index = other.index;
		return *this;
	}

	bool operator > (const GameObjectSorter & other) const {
		return this->cost > other.cost;
	}
};

bool compareGameObject(GameObjectSorter &a, GameObjectSorter &b) {
	return a.cost < b.cost;
}

#endif