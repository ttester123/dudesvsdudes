#ifndef GOGRAPHICS_H
#define GOGRAPHICS_H

#include "../../../GameObjects/GameObject.h"

class World;

/// Abstract Component Class
class GoGraphics{
protected:
	World *world;
public:
	virtual ~GoGraphics();
	/**
	 *Abstract function for initialization.
	*/
	virtual bool init( World *world )	= 0;
	
	/**
	*Abstract update function.
	*/
	virtual void update(GameObject *go) = 0;
	
	/**
	*Abstract function for rendering to a world object.
	*
	*\param world                       = Pointer to the world object.
	*/
	virtual void render()   = 0;
};

#endif