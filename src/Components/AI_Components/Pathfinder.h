#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "../../GameObjects/Point.h"
#include "CostGrid.h"

class Pathfinder {
public:
	virtual bool findPath()=0;
	virtual Point next()=0;
	virtual bool hasNext()=0;
	virtual void setCostGrid(CostGrid)=0;
};

#endif