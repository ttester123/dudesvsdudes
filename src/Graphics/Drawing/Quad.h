#ifndef QUAD_H
#define QUAD_H

class World;
#include "AQState.h"
#include <vector>

class Quad{
protected:
	std::vector<AQState> states;
public:
	virtual ~Quad();
	virtual void render(AQState state) = 0;
	virtual void applyState(AQState newState);
	virtual AQState getState(int index);
	virtual void pushToDeck(AQState newState, World *world);
	virtual void pushToDeck(GLfloat x, GLfloat y, GLuint index, World *world);
	virtual void pushToDeck(int index, World *world);

	//Use to update a specific state in the corresponding index.
	virtual bool updateState( int index, AQState newState );
};

#endif