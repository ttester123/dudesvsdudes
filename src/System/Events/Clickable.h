#ifndef CLICKABLE_H
#define CLICKABLE_H

#include "Observer.h"
#include "MouseInput.h"
#include "../LFRect.h"
#include "ScreenPauseEvent.h"

class World;

class Clickable: public Observer{
protected:
	bool active;
	bool checkClick();
	LFRect clickBox;
	World *world;
	virtual void onClick() = 0;
public:
	Clickable();
	Clickable( World *world );
	virtual ~Clickable();
	virtual void setWorld(World *world);
	virtual void setClickBox(int x, int y, int w, int h);
	virtual void setClickBox( LFRect box );
	LFRect getClickBox();
	bool isActive();
	void setActive( bool active );
	virtual void notify( Event event );
};

#endif