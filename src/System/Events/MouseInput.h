#ifndef MOUSEINPUT_H
#define MOUSEINPUT_H

#include "Subject.h"

class MouseInput{
public:
	virtual ~MouseInput();
	static Subject subject;
	static void getMouseState( int &x, int &y, bool relative = true );
	static void handleEvents( SDL_Event );
	static Event determineMouseEvent( SDL_Event event );
};

#endif