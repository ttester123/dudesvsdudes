#ifndef TBUTTON_H
#define TBUTTON_H

#include "../System/Events/Hoverable.h"

class TButton: public Hoverable{
public:
	virtual void onClick();
	virtual void onHover();
};

#endif