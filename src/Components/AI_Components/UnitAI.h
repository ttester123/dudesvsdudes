#ifndef UNITAI_H
#define UNITAI_H

#include "Ai.h"
#include "AStar.h"
#include "TargetFinder.h"
#include "CostGrid.h"
#include "Action.h"
#include "../../GameObjects/Point.h"
#include "../../GameObjects/Board.h"
#include "../../GameObjects/GameObject.h"
#include "../../GameObjects/Unit.h"
#include "../../GameObjects/GameObjectPtr.h"

class UnitAI {
protected:
	Unit *self;

	AStar pathfinder;
	int pathfinderTry;
	bool stuck;
	bool pathdone;
	Point rallyPoint;
	int range;
	GameObjectPtr target;
	TargetFinder finder;
	CostGrid *grid;
	Action primary;
	Action secondary;

	void followPath();
	void findTarget(Point end);
	void followRally();
	bool attack(Unit *victim);

public:
	UnitAI();
	/**
	 * Update function. Directs the unit on what to do next
	 */
	virtual void update();

	/**
	 * Initializes AI
	 */
	virtual bool init(Board *b,int team, Unit *self);

	/**
	 * Sets the rally point of this unit
	 *\param p = new rally point
	 *\param self = Unit of this ai
	 *\param grid = grid to pathfind from
	 */
	void setRallyPoint(Point p, CostGrid *grid);

	void setAction(Action action);

	/**
	 * Returns true if at end of pathfinding.
	 * This is not connected to being at rally point.
	 */
	bool atPathEnd();

	/**
	 * cleans up pointers
	 */
	void cleanUp();
	friend class Unit;
	friend class Squad;
};

#endif