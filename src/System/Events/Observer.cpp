#include "Observer.h"
#include "Subject.h"

bool Observer::compare( Event &a, Event &b) {
	return int(a.getType()) > int(b.getType());
}

Observer::Observer(){
	eventHeap = NULL;
	eventHeap = new Heap<Event>(&compare);
}

Observer::~Observer(){
	eventHeap->clear();
	delete eventHeap;
	removeLogs();

	for(int i = 0; i < logged.size(); ++i){
		delete logged[i].second;
	}
}

void Observer::notify(Event event){
	eventHeap->push( event );
}

void Observer::removeLogs(){
	for(int i = 0; i < logged.size(); ++i){
		logged[i].first->removeObserver(this, logged[i].second);
	}
	for(int i = 0; i < loggedToAll.size(); i++){
		if( loggedToAll[i] != NULL )
			loggedToAll[i]->removeObserver(this);
	}
}

void Observer::addToLog( Subject &sub, Event event){
	logged.push_back( std::make_pair(&sub, &event) );
	logged[ logged.size() - 1 ].first->addObserver(this, logged[ logged.size() - 1 ].second);
}
void Observer::addToLog( Subject &sub, Event *event){
	logged.push_back( std::make_pair(&sub, event) );
	logged[ logged.size() - 1 ].first->addObserver(this, logged[ logged.size() - 1 ].second);
}
void Observer::addToLog( Subject &sub, Events event){
	logged.push_back( std::make_pair(&sub, new Event(event) ) );
	logged[ logged.size() - 1 ].first->addObserver(this, logged[ logged.size() - 1 ].second);
}

void Observer::addToLog( Subject &sub ){
	sub.addObserver( this );
	loggedToAll.push_back( &sub );
}

void Observer::push_event( Event event ){
	eventHeap->push( event );
}

Event Observer::pop_event(){
	return eventHeap->pop();
}

int Observer::getSizeEvents(){
	return eventHeap->getSize();
}