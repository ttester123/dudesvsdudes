#ifndef EXTENDEDEVENT_H
#define EXTENDEDEVENT_H

#include "Event.h"
#include "../../GameObjects/GameObjectPtr.h"
#include "../../GameObjects/Point.h"

class ExtendedEvent :public Event {
public:
	GameObjectPtr dude;
	Point p;
	int data1;
	int data2;

	ExtendedEvent(Events type, GameObjectPtr dude, int data1=0, int data2=0);

	ExtendedEvent(Events type, Point p, int data1=0, int data2=0);

	ExtendedEvent(Events type, int data1=0, int data2=0);
};

#endif