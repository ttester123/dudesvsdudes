#ifndef PATHFINDERHEAP_H
#define PATHFINDERHEAP_H

#include "Pathfinder.h"
#include "PathfinderPointer.h"
#include "../../System/Heap.h"
#include "../../GameObjects/GameObjectPtr.h"

class PathfinderHeap {
private:
	//Used for comparisons during sorting. Use when intializing with make_heap();
	bool comparison(PathfinderPointer &a, PathfinderPointer &b);

	//Heap where nodes are stored
	Heap<PathfinderPointer> *heap;

	//Clears the heap, removing all nodes.
	void clear();
	
public:
	//Initialize heap
	PathfinderHeap();

	//Destroy heap
	~PathfinderHeap();

	//push node onto heap
	void push(PathfinderPointer p);

	//pop node with lowest cost
	GameObjectPtr pop();

	//Gets the size of the heap.
	int getSize();

	//erase data in heap
	void erase();

	//Updates heap based on object that has changed
	void update(GameObjectPtr n);

	//Return true if empty
	bool isEmpty();
};

#endif