#ifndef CONFIG_H
#define CONFIG_H

#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "../StringManip.h"

class Config{
protected:
	std::map< std::string, std::string > 	strMap;
	std::map< std::string, int >			intMap;
	std::map< std::string, float >			floatMap;
public:
	virtual ~Config();

	/**
	 * Loads the config file.
	 * \param path = The path to the file to load.
	 */
	bool load( std::string path );

	/**
	 * \param Name = The string to use as a key when searching for the value.
	 * \return A string value that was loaded. Returns "" if not found.
	 */
	std::string getString( std::string name );

	/**
	 * \param name = The string to use as a key when searching for the value.
	 * \return An int value that was loaded. Returns 0 if not found.
	 */
	int getInt( std::string name );

	/**
	 * \param name = The string to use as a key when searching for the value.
	 * \return A float value that was loaded. Returns 0.0 if not found.
	 */
	float getFloat( std::string name );
};

#endif