#ifndef EXPLOSION_H
#define EXPLOSION_H

#include "GameObject.h"
#include "Point.h"
#include "../Graphics/Animated.h"
#include "../System/LOpenGL.h"

class Explosion: public GameObject {
	Animated *splosion;

	Point location;

public:
	~Explosion();

	bool init(GLfloat x, GLfloat y, Point);

	void update(World *world);

	void render(World *world);
};

#endif