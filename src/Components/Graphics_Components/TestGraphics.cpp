#include "TestGraphics.h"

TestGraphics::~TestGraphics(){
	delete anim;
}

bool TestGraphics::init(){
	anim = new Animated;

	if( anim->loadAnimation("res/Animations/grunt.anim") == false ){
		return false;
	}
	return true;
}

void TestGraphics::update(){
	anim->update();
}

void TestGraphics::render(World *world){
	anim->draw(world, 100,100);
}