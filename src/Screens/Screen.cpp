#include "Screen.h"
#include "ScreenManager.h"

Screen::Screen(){
	active = false;
	scrolling = false;
}

Screen::Screen(ScreenManager *sm){
	setSM( sm );
}

Screen::~Screen(){}

void Screen::setSM( ScreenManager *sm ){
	addToLog( sm->subject );
}

bool Screen::isActive(){
	return active;
}

void Screen::setActive(bool value){
	active = value;

	Event event;

	if( active == true ){
		event.setEvent( SCREEN_UNPAUSE );
	}
	else{
		event.setEvent( SCREEN_PAUSE );
	}
	this->subject.push_event( event );
	this->subject.notifyObservers();
}

void Screen::notify(Event event){
	if( isActive() == true ){
		push_event( event );
	}
}

bool Screen::isScrolling(){
	return scrolling;
}