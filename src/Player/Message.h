#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>

using std::string;

struct Message {
	string text;
	int x;
	int y;
	int start;
	int end;
};

#endif