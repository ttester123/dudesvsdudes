@echo off
set OBJDIR=ObjectFolder
set SCRIPTDIR=MakeMaker
set EXENAME=DvD.exe
set SCRIPT=MakeMakerScriptWin.bat
set MAKEFILE=Makefile.win

if exist %OBJDIR% rmdir /s/q %OBJDIR%

mkdir %OBJDIR%

if exist %EXENAME% del %EXENAME%

cd %SCRIPTDIR%
call %SCRIPT%
cd ..
make -s -f %MAKEFILE%