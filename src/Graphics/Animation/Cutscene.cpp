#include "Cutscene.h"
#include "../../System/World.h"

Cutscene::Cutscene(World *world){
	setWorld( world );
	initVars();
}

Cutscene::~Cutscene(){}

bool Cutscene::init( std::string path ){
	return loadCutscene( path );
}

void Cutscene::setWorld( World *world ){
	this->world = world;
}

void Cutscene::initVars(){
	timeRange.startTime = 0;
	timeRange.endTime = -1;
	beginTime = 0;

	started = false;
	finished = false;
}

bool Cutscene::update(){
	if( isStarted() == false || isFinished() == true ){
		start();
	}
	bool finishAnim = true;
	GLuint currentTime = (Constants::getFrames() - beginTime);
	if( currentTime >= timeRange.startTime && currentTime <= timeRange.endTime + timeRange.startTime ){
		for(int i = 0; i < anims.size(); i++){
			if( currentTime >= anims[i].first.startTime && currentTime <= anims[i].first.endTime  ){
				finishAnim = false;
				if( anims[i].second.update() == true && anims[i].first.endTime == -2 && anims[i].second.getCurrAnim()->isLoop() == false ){
					anims[i].first.endTime = currentTime;
				}
			}
		}
	}
	if( finishAnim == true ){
		stop();
		return true;
	}
	return false;
}

void Cutscene::render(){
	GLuint currentTime = (Constants::getFrames() - beginTime);
	if( currentTime >= timeRange.startTime ){
		for(int i = 0; i < anims.size(); i++){
			if( currentTime >= anims[i].first.startTime && currentTime <= anims[i].first.endTime ){
				anims[i].second.draw(world);
			}
		}
	}
}

void Cutscene::start(){
	started = true;
	finished = false;
	beginTime = Constants::getFrames();
	for(int i = 0; i < anims.size(); i++){
		anims[i].second.reset();
	}
}

void Cutscene::stop(){
	started = false;
	finished = true;
	timeRange.startTime = 0;
	beginTime = 0;
}

bool Cutscene::isStarted(){
	return started;
}

bool Cutscene::isFinished(){
	return finished;
}

bool Cutscene::loadCutscene( std::string path ){
	if( world == NULL ){
		return false;
	}

	std::ifstream file( path.c_str() );
	if( file.is_open() == true && file.eof() == false ){
		

		std::string line = "";
		getline( file, line);

		int found = line.find("//");
		if( found != -1 ){
			line = line.substr(0, found);
		}

		if( line == "" ){
            return false;
        }

		std::vector<std::string> values = StringManip::split( line, ",");



		if( values.size() < 1 ){
			return false;
		}
		for(int i = 0; i < values.size(); i++){
			values[i] = StringManip::removeWhitespace( values[i] );
		}

		if( values[0] != "start" ){
			return false;
		}

		timeRange.startTime = 0;
		timeRange.endTime   = -1;
		switch( values.size() ){
			case 3:
				timeRange.endTime   = atoi( values[2].c_str() );
			case 2:
				timeRange.startTime = atoi( values[1].c_str() );
		}


		TimeRange tr = {0,0};
		while( getline(file, line) ){

			found = line.find("//");
			if( found != -1 ){
				line = line.substr(0, found);
			}

			if( line == "" ){
		        continue;
		    }

			values = StringManip::split( line, ",");
			if( values.size() < 1 ){
				break;
			}
			for(int i = 0; i < values.size(); i++){
				values[i] = StringManip::removeWhitespace( values[i] );
			}

			tr.startTime = 0;
			tr.endTime = -1;
			
			int index = 0;

			AQState delta;
			delta.clear();

			AnimIter iter;
			switch( values.size() ){
				case 6:
					delta.y = atof( values[5].c_str() );
				case 5:
					delta.x = atof( values[4].c_str() );
				case 4:
					tr.endTime = atoi( values[3].c_str() );
				case 3:
					tr.startTime = atoi( values[2] .c_str() );
				case 2:
					index = atoi(values[1].c_str());
				case 1:
					iter = world->af.getIter( values[0] );
			}
			if( iter.isLoaded() == true ){
				iter.switchAnim( index );
				iter.addDelta( delta );
				anims.push_back( std::make_pair( tr, iter) );
			}
			else{
				printf("Cutscene Error: Failed to load animation %s\n", values[0].c_str());
			}
		}
	}
	return true;
}