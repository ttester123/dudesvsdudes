#include "UnitAI.h"
#include "AStar.h"
#include "Pathfinder.h"
#include "Dijkstra.h"
#include "../../GameObjects/Point.h"
#include "../../GameObjects/Board.h"
#include "../../GameObjects/GameObject.h"
#include "../../GameObjects/Unit.h"
#include "../../System/LOpenGL.h"

UnitAI::UnitAI() {
	self = NULL;
	pathfinderTry = 0;
	stuck = false;
	pathdone = false;
	Point rallyPoint = Point();
	range = 5;
	grid = NULL;
}

bool UnitAI::init(Board *b, int team, Unit *self) {
	this->self = self;
	pathfinder.setCostGrid(b->getCostGrid());
	pathfinderTry = 1;
	pathdone = false;
	finder = TargetFinder(b);
	finder.setRange(range);
	finder.setTeam(team);
	primary = Action();
	return true;
}

void UnitAI::update() {
	if(primary.update()) return;

	if(self->stationary) return;

	if(self->location.distance(rallyPoint) > range + self->attackRange) {
		//if not within destination range
		GameObjectPtr temp = finder.findNextTo(self->location);
		Unit* tempTarget = dynamic_cast<Unit*>(temp.getPtr());
		if(tempTarget != NULL) {
			//there is a target next to me
			target = temp;
			attack(tempTarget);
		} else {
			//nobody next to, go home
			followRally();
			//std::cout << "going home\n";
		}
	} else {
		//at destination
		//std::cout << "At rally\n";
		Unit* targetCast = dynamic_cast<Unit*> (target.getPtr());
		if(targetCast != NULL) {
			//target exist
			//std::cout << "Have target\n";
			if(!attack(targetCast)) {
				//so not next to target
				if(targetCast->location.distance(rallyPoint) > range + self->attackRange) {
					//std::cout << "too far\n";
					GameObjectPtr temp = finder.findNextTo(self->location);
					Unit* tempTarget = dynamic_cast<Unit*>(temp.getPtr());
					if(tempTarget != NULL) {
						//there is a target next to me
						target = temp;
						attack(tempTarget);
						//std::cout << "attacking\n";
					} else {
						//nobody next to, go home
						target = finder.find();
						//if(pathfinder.getEnd() != rallyPoint)
						//	findTarget(self,rallyPoint);
						//followPath(self);
						followRally();
					}
				} else {
					//target is in range
					//std::cout << "in range\n";
					GameObjectPtr temp = finder.findNextTo(self->location);
					Unit* tempTarget = dynamic_cast<Unit*>(temp.getPtr());
					if(tempTarget != NULL) {
						//there is a target next to me
						target = temp;
						attack(tempTarget);
						//std::cout << "nextto target found\n";
					} else
					//not next to target but target is close enough to fight
					if(pathfinder.getEnd() == targetCast->location ||
						pathfinder.getEnd() == targetCast->nextLocation) {
						//std::cout << "following path\n";
						followPath();
					} else {
						//std::cout << "finding path\n";
						findTarget(targetCast->location);
					}
				}
			}
		} else {
			target = finder.findNextTo(self->location);
			if(target.getPtr() == NULL)
				target = finder.find();
			followRally();
		}
	}
}

void UnitAI::followPath() {
	//std::cout << "Following target\n";
	if(self->atTarget == true) {
		//std::cout << "Next point\n";
		if(pathfinder.hasNext() == true) {
			if(pathdone == true) {
				pathdone = false;
				//if(self->board->getCost(self->location) != Board::WALL)
				//	self->board->setCost(self->location, 1);
			}
			Point next = pathfinder.safeNext();
			if(	self->board->getGameObject(next).getPtr() == NULL &&
				self->board->isOpen(next) == true) {

				pathfinder.setStart(next);
				if(self->board->getCost(self->location) != Board::WALL)
					self->board->setCost(self->location, 1);
				self->moveTo(pathfinder.next());
			} else {
				Point start;
				start = self->nextLocation;
				if(start == Point(-1,-1)) start = self->location;
				pathfinder.setStart(start);
				if(pathfinder.pathFound == true) {
					self->board->pushPathfinder(PathfinderPointer(self->me,
						start.distance(pathfinder.getEnd()) + (pathfinderTry+=2)));
					pathfinder.pathFound = false;
				}
				if(self->board->getGameObject(next).getPtr() != NULL &&
					self->board->getCost(self->location) != Board::WALL) {
					//self->board->setCost(self->location, 2);
				}
			}
		} else {
			pathdone = true;
			if(self->board->getCost(self->location) != Board::WALL)
				self->board->setCost(self->location, 10);
		}
	}
}

void UnitAI::followRally() {
	//std::cout << "finding route home\n";
	if(self->atTarget == true) {
		if(self->board->getCost(self->location) != Board::WALL)
			self->board->setCost(self->location, 1);
		self->moveTo(nextPoint(self->board,*grid,self->location,self->lastLocation));
	}
}

bool UnitAI::attack(Unit *victim) {
	if(victim == NULL) return false;
	if(!primary.done()) return false;
	if(self->attack(victim->location)) {
		primary = Action(ATTACK,self->attackCooldown);
		return true;
	}
	return false;
}

void UnitAI::setRallyPoint(Point end, CostGrid *grid) {
	this->grid = grid;
	rallyPoint = end;
	finder.setCenter(end);
}

void UnitAI::findTarget(Point end) {
	Point start;
	start = self->nextLocation;
	if(start == Point(-1,-1)) start = self->location;
	self->board->pushPathfinder(PathfinderPointer(self->me,start.distance(end)));
	pathfinder.setStart(start);
	pathfinder.setEnd(end);
	pathfinder.pathFound = false;
}

bool UnitAI::atPathEnd() {
	return pathdone;
}

void UnitAI::cleanUp() {
	if(self->board->getCost(self->location) != Board::WALL)
		self->board->setCost(self->location, 1);
}