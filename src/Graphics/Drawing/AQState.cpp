#include "AQState.h"

//Basic initialization of the elements.
AQState::AQState(){
	resetToDefault();
}

void AQState::resetToDefault(){
	x        = 0.f;
	y        = 0.f;
	w        = 0.f;
	h        = 0.f;
	index    = 0;
	layer    = 0;
	priority = 0;
	degrees  = 0.f;
	scaleX   = 1.f;
	scaleY   = 1.f;
	scale    = 1.f;
	text     = "";
	drawFromCenter = false;
	drawAbsolute = false;
}

void AQState::clear(){
	x        = 0.f;
	y        = 0.f;
	w        = 0.f;
	h        = 0.f;
	index    = 0;
	layer    = 0;
	priority = 0;
	degrees  = 0.f;
	text     = "";

	scaleX   = 0.f;
	scaleY   = 0.f;
	scale    = 0.f;
	color.r  = 0;
	color.g  = 0;
	color.b  = 0;
	color.a  = 0;

	drawFromCenter = 0;
	drawAbsolute = 0;
}

AQState AQState::operator+(AQState state){
	AQState newState;
	newState.x              = this->x 				+ state.x;
	newState.y              = this->y 				+ state.y;
	newState.w              = this->w 				+ state.w;
	newState.h              = this->h 				+ state.h;
	newState.index          = this->index 			+ state.index;
	newState.layer          = this->layer 			+ state.layer;
	newState.priority       = this->priority 		+ state.priority;
	newState.degrees        = this->degrees 		+ state.degrees;
	newState.scaleX         = this->scaleX 			+ state.scaleX;
	newState.scaleY         = this->scaleY 			+ state.scaleY;
	newState.scale          = this->scale 			+ state.scale;
	newState.text           = this->text 			+ state.text;
	newState.color.r        = this->color.r  		+ state.color.r;
	newState.color.g        = this->color.g   		+ state.color.g;
	newState.color.b        = this->color.b   		+ state.color.b;
	newState.color.a        = this->color.a   		+ state.color.a;
	newState.drawFromCenter = this->drawFromCenter 	|| state.drawFromCenter;
	newState.drawAbsolute 	= this->drawAbsolute 	|| state.drawAbsolute;
	return newState;
}

AQState AQState::operator*(int times){
	AQState newState;
	newState.x              = this->x 				* times;
	newState.y              = this->y 				* times;
	newState.w              = this->w 				* times;
	newState.h              = this->h 				* times;
	newState.index          = this->index 			* times;
	newState.layer          = this->layer 			* times;
	newState.priority       = this->priority 		* times;
	newState.degrees        = this->degrees 		* times;
	newState.scaleX         = this->scaleX 			* times;
	newState.scaleY         = this->scaleY 			* times;
	newState.scale          = this->scale 			* times;
	newState.color.r        = this->color.r  		* times;
	newState.color.g        = this->color.g   		* times;
	newState.color.b        = this->color.b   		* times;
	newState.color.a        = this->color.a   		* times;
	return newState;
}

void AQState::operator+=(AQState delta){
	*this = *this + delta;
}

void AQState::operator*=(int times){
	*this = *this * times;
}
